<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Register and Auth
Route::get('logout', 'UserController@logout');
Route::post('register', 'UserController@store');
Route::post('auth', 'UserController@auth');
Route::get('index', 'UserController@index');
Route::get('login', 'UserController@login');
Route::post('verifysms', 'UserController@sms_verify');
Route::post('verifyemail', 'UserController@email_verify');

//Dashboard
Route::get('dashboard','VideoController@dashboard');

//CRUD Video 
Route::resource('video','VideoController');

//CRUD Profile
Route::resource('profile','ProfileController');

//Videos
Route::post('video', 'VideoController@store');
Route::post('/search','VideoController@search');
Route::get('/video/{id}/delete', 'VideoController@destroy');

//Profiles
Route::get('/profile/{id}/delete', 'ProfileController@destroy');



