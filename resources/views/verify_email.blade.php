@extends('layouts.layout')
@section('content')
<div class="container">
    @include('flash::message')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification code has been sent to your email address.') }}
                        </div>

                    {{ __('Before proceeding, please check your email for a verification code.') }}
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ url('verifyemail') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="confirmation_code" class="col-sm-4 col-form-label text-md-right">{{ __('Code') }}</label>

                            <div class="col-md-6">
                                <input id="confirmation_code" type="text" class="form-control{{ $errors->has('confirmation_code') ? ' is-invalid' : '' }}" name="confirmation_code" value="{{ old('confirmation_code') }}" required autofocus>

                                @if ($errors->has('confirmation_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('confirmation_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
