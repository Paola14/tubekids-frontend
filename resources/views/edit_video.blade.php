@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['url' => '/video/'.$video->id, 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Edit Video</h2>
              			<div class="card-header">
                			<div class="row">
                  				<div class="col-md-8">
                  					<div class="row">
							          <div class="col-md-4"></div>
							         	<div class="form-group col-md-4">
							            	<label for="name">Name:</label>
							            	<input type="text" class="form-control" name="name" value="{{$video->name}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="path">Id Path:</label>
							            	<input type="text" class="form-control" name="path" value="{{$video->path}}">
							          	</div>
							          	
							        </div>
							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Update</button>
							          </div>
							        </div>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection