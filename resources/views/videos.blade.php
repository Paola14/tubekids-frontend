@extends('layouts.app')

@section('content')
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">Videos Form
                        </div>
                        <div class="card-body">
                            {!! Form::open(['url' => '/video', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('path', 'Id Path') !!}
                                {!! Form::text('path', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                            </div>

                        
                            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}

                        {!! Form::close() !!}

                        </div>

                    </div>
                </div>

            </div>
            
        </div>
@endsection