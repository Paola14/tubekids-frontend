@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('/profile/create') }}"><button type="button" class="btn btn-secondary btn-lg">Add Profile</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Profiles List 
                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Username</th>
                      <th scope="col">Pin</th>
                      <th scope="col">Age</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($profiles as $key)  
                          <tr>
                            <td scope="row">{{ $key->name }}</td>
                            <td scope="row">{{ $key->username }}</td>
                            <td scope="row">{{ $key->pin }}</td>
                            <td scope="row">{{ $key->age }}</td>
                            <td>
                              <a href="/profile/{{ $key->id }}/edit" id="{{ $key->id }}" class="btn btn-primary">
                                <i class= "icon-pencil"></i>Edit</a>
                              <a href="/profile/{{ $key->id }}/delete" id="{{ $key->id }}" class="btn btn-danger">
                                <i class= "icon-trash"></i>Delete</a>
                            </td>
                          </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection