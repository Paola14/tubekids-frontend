@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['url' => '/profile/'.$profile->id, 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Edit profile</h2>
              			<div class="card-header">
                			<div class="row">
                  				<div class="col-md-8">
                  					<div class="row">
							          <div class="col-md-4"></div>
							         	<div class="form-group col-md-4">
							            	<label for="name">Name:</label>
							            	<input type="text" class="form-control" name="name" value="{{$profile->name}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="username">Username:</label>
							            	<input type="text" class="form-control" name="username" value="{{$profile->username}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="pin">PIN:</label>
							            	<input type="text" class="form-control" name="pin" value="{{$profile->pin}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="edad">Age:</label>
							            	<input type="text" class="form-control" name="age" value="{{$profile->age}}">
							          	</div>
							        </div>
							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Update</button>
							          </div>
							        </div>

                  				</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection