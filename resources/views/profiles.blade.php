@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">Profiles Forms
                        </div>
                        <div class="card-body">
                            {!! Form::open(['url' => '/profile', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('username', 'Username') !!}
                                {!! Form::text('username', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('pin', 'PIN') !!}
                                {!! Form::number('pin', '', ['placeholder' => '', 'class' => 'form-control','min' => '0',
                             'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('age', 'Age') !!}
                                {!! Form::number('age', '', ['placeholder' => '', 'class' => 'form-control','min' => '0', 'required']) !!}
                            </div>
                            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}

                        {!! Form::close() !!}

                        </div>

                    </div>
                </div>

            </div>
            
        </div>
@endsection