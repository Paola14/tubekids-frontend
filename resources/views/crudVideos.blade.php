@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('/video/create') }}"><button type="button" class="btn btn-secondary btn-lg">Add Video</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Videos List
                  </div>
                  <div class="col-md-8">
                    <form action="/search" method="post">
                      <div class="input-group">
                        <input type="search" name="name" class="form-control">
                        <span class="input-group-prepend">
                          <button type="submit" class="btn btn-primary">Search</button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Video</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($videos as $key)  
                          <tr>
                            <td scope="row">{{ $key->name }}</td>
                            <td scope="row">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $key->path }}" frameborder="0" allowfullscreen></iframe>
                            </td>
                            <td>
                              <a href="/video/{{ $key->id }}/edit" id="{{ $key->id }}" class="btn btn-primary">
                                <i class= "icon-pencil"></i>Edit</a>
                              <a href="/video/{{ $key->id }}/delete" id="{{ $key->id }}" class="btn btn-danger">
                                <i class= "icon-trash"></i>Delete</a>
                            </td>
                          </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection