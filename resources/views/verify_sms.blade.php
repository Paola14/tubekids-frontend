@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verification User') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('verifysms') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="pin_code" class="col-sm-4 col-form-label text-md-right">{{ __('Code') }}</label>

                            <div class="col-md-6">
                                <input id="pin_code" type="text" class="form-control{{ $errors->has('pin_code') ? ' is-invalid' : '' }}" name="pin_code" value="{{ old('pin_code') }}" required autofocus>

                                @if ($errors->has('pin_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pin_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

