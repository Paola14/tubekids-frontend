<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends ClientController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $response = json_decode($this->listVideos());
            if (strval($response->code) == 200) {
                return view('crudVideos')->with('videos', $response->videos);
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function search(Request $request)
    {
            $response = json_decode($this->searchVideos($request));
            if (strval($response->code) == 201) {
                return view('crudVideos')->with('videos', $response->video);
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('videos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request['token'] = session('token');
            $response = json_decode($this->createVideo($request));
            if (strval($response->code) == "201") {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = json_decode($this->showVideo($id));
        
            if (strval($response->code) == 200) {
                return view('edit_video')->with('video', $response->video);
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['token'] = session('token');
        $request['id'] = $id;
            $response = json_decode($this->updateVideo($request));
            if (strval($response->code) == 200) {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request['token'] = session('token');
        $request['id'] = $id;
            $response = json_decode($this->deleteVideo($request));
            if (strval($response->code) == 200) {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }
}
