<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ClientController extends Controller
{
  //Route Register Client
  protected function registerUser($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/register', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route Verify email Client
  protected function verifyEmail($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/verifyemail', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route Login Client
  protected function loginUser($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/login', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route Verify sms Client
  protected function verifySms($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/verifysms', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route Logout Client
   protected function logoutUser($token)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/logout/'.$token);
    return $response->getBody()->getContents();
  }

  //Profiles 

  //Route create Profile Client
  protected function createProfile($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/profiles', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route list Profiles Client
  protected function listProfiles()
  {
    $request['token'] = session('token');
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/get_profiles', [
          'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  //Route edit view Profile Client
  protected function showProfile($id)
  {
    $client = new Client();
    $response = $client->get('http://localhost:8000/api/profiles/'.$id);
    return $response->getBody()->getContents();
  }

  //Route update Profile Client
  protected function updateProfile($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/profiles', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route delete Profile Client
  protected function deleteProfile($request)
  {
    $client = new Client();
    $response = $client->delete('http://localhost:8000/api/profiles/'. $request['id'], [
        'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  //Videos 

   //Route create Video Client
  protected function createVideo($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/videos', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route list of Video Client
  protected function listVideos()
  {
    $request['token'] = session('token');
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/get_videos', [
          'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  //Route search Video Client
  protected function searchVideos()
  {
    $request['token'] = session('token');
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/search', [
          'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  //Route edit view video Client
  protected function showVideo($id)
  {
    $client = new Client();
    $response = $client->get('http://localhost:8000/api/videos/'.$id);
    return $response->getBody()->getContents();
  }

  //Route update Video Client
  protected function updateVideo($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/videos', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  //Route delete Video Client
  protected function deleteVideo($request)
  {
    $client = new Client();
    $response = $client->delete('http://localhost:8000/api/videos/'. $request['id'], [
        'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }
}