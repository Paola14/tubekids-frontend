<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends ClientController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('registerUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $token = session('token');
        $response = json_decode($this->logoutUser($token));
        return view('welcome');
    }

/**
     * Login
     *
     * @return \Illuminate\Http\Response
     */
    public function auth(Request $request)
    {
        $response = json_decode($this->loginUser($request));
            if ($response->code == 200) {
                session(['token' => $response->token]);
                return view('verify_sms');
            }else{
                flash($response->message)->error();
                return back()->withInput();
            }
    }

    /* Method for verify a user */

    public function sms_verify(Request $request)
    {
        $response = json_decode($this->verifySms($request));
            if ($response->code == 200) {
                return redirect()->action('VideoController@dashboard');
            }else{
                flash($response->message)->error();
                return back()->withInput();
            }
    }


    /**
     * Store a newly register
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['password_confirmation'] == $request['password'] ){
            $response = json_decode($this->registerUser($request));
            if (strval($response->code) == 201) {
                return view('verify_email');
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
        }else{
            flash('The passwords don`t match')->error();;
            return back()->withInput();
        }
    }

    /* Method for verify a register */

    public function email_verify(Request $request)
    {
        $response = json_decode($this->verifyEmail($request));
            if ($response->code == 200) {
                return view('login');
            }else{
                flash($response->message)->error();
                return back()->withInput();
            }
    }

}
